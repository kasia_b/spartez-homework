import PageWithPages from "./pageWithPages";

class PageViewerPage extends PageWithPages {

    constructor() {
        super('.PageTree_anchor_20K');
    }

    get editButton() {
        return '#editPageLink';
    }

    get mainContent() {
        return '#main-content';
    }

    openEditMode() {
        this.waitForVisibleAndClick(this.editButton);
    }

    canEdit() {
        return browser.isVisible(this.editButton);
    }

    getContent() {
        return this.waitForVisibleAndGetText(this.mainContent);
    }
}

export default new PageViewerPage();