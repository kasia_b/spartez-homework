import Page from "./page";

class PageWithPages extends Page {

    constructor(pagesSelector, title) {
        super(title);
        this.pagesSelector = pagesSelector;
    }

    getListOfPages() {
        this.waitUntilVisible(this.pagesSelector);
        return browser.elements(this.pagesSelector).value.map(row => row.getAttribute('textContent'));
    }
}

export default PageWithPages;