import PageWithPages from "./pageWithPages";

class SpacePage extends PageWithPages {

    constructor(space) {
        super('.link-title', space.name + ' - Confluence');
        this.space = space;
    }

    getListOfPages() {
        this.open('/wiki/spaces/' + this.space.key + '/pages');
        return super.getListOfPages();
    }
}

export default SpacePage;