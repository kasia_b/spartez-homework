const Restriction = {
    DEFAULT: 0,
    NOEDIT: 1,
    NOVIEW: 2

};

class ConfluencePage {

    constructor(restriction, title, content) {
        let rand = (new Date() / 1000);
        this.title = title || 'Julian ' + rand;
        this.content = content || 'kotek' + rand;
        this.restriction = restriction || Restriction.DEFAULT;
    }
}

export {Restriction, ConfluencePage} ;