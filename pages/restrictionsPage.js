import Page from './page';

class RestrictionsPage extends Page {

    get restictionsButton() {
        return '#rte-button-restrictions';
    }

    get updateRestrictionsSelector() {
        return '#s2id_page-restrictions-dialog-selector';
    }

    get selectRestrictionSelect() {
        return '#page-restrictions-dialog-selector';
    }

    get selectRestrictionOptions() {
        return 'div.restrictions-dialog-option';
    }

    get applyRestrictionsButton() {
        return '#page-restrictions-dialog-save-button';
    }

    setRestriction(restriction) {
        this.waitForVisibleAndClick(this.restictionsButton);
        this.waitUntilVisible(this.updateRestrictionsSelector);
        browser.selectByIndex(this.selectRestrictionSelect, restriction);
        this.waitForVisibleAndClick(this.applyRestrictionsButton);
        this.waitUntilVisible(this.updateRestrictionsSelector, undefined, true)
    }
}

export default new RestrictionsPage();