export default class Page {
    constructor(title) {
        this.defaultTimeout = 120000;
        this.title = title;
    }

    open(path) {
        browser.url(path);
        this.waitUntilTitle(this.title);
    }

    waitAndAction(wait, action, selector, timeout, reverse) {
        wait(selector, timeout || this.defaultTimeout, reverse);
        let result = action(selector);
        return result || this;
    }

    waitUntilExists(selector, timeout, reverse) {
        return this.waitForExistAndAction((selector) => {
        }, selector, timeout, reverse);
    }

    waitUntilVisible(selector, timeout, reverse) {
        return this.waitForVisibleAndAction((selector) => {
        }, selector, timeout, reverse);
    }

    waitForVisibleAndGetText(selector, timeout, reverse) {
        return this.waitForVisibleAndAction((selector) => $(selector).getText(), selector, timeout, reverse);
    }

    waitForEnabledAndAction(action, selector, timeout, reverse) {
        return this.waitAndAction((selector, timeout, reverse) => $(selector).waitForEnabled(timeout, reverse), action, selector, timeout, reverse);
    }

    waitForExistAndAction(action, selector, timeout, reverse) {
        return this.waitAndAction((selector, timeout, reverse) => $(selector).waitForExist(timeout, reverse), action, selector, timeout, reverse);
    }

    waitForEnabledAndClick(selector, timeout, reverse) {
        return this.waitForEnabledAndAction((selector) => $(selector).click(), selector, timeout, reverse);
    }

    waitForVisibleAndAction(action, selector, timeout, reverse) {
        return this.waitAndAction((selector, timeout, reverse) => $(selector).waitForVisible(timeout, reverse), action, selector, timeout, reverse);
    }

    waitForVisibleAndSet(selector, value, timeout, reverse) {
        return this.waitForVisibleAndAction((selector) => $(selector).setValue(value), selector, timeout, reverse);
    }

    waitForVisibleAndClick(selector, timeout, reverse) {
        return this.waitForVisibleAndAction((selector) => $(selector).click(), selector, timeout, reverse);
    }

    waitForAndNavigateToFrame(selector, timeout, reverse) {
        if (selector) {
            return this.waitForExistAndAction((selector) => {
                let frame = $(selector).value;
                browser.frame(frame);
            }, selector, timeout, reverse);
        }
        browser.frame();
    }

    waitUntilTitle(title, timeout) {
        browser.waitUntil(
            () => {
                return browser.getTitle() === title;
            }, timeout || this.defaultTimeout);
    }
}
