import Page from './page';

class PageCreatorPage extends Page {

    get blankPage() {
        return 'li[data-item-module-complete-key="com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blank-page"]';
    }

    get createPage() {
        return '.create-dialog-create-button';
    }

    get createPageDialog() {
        return 'h2.dialog-title';
    }

    chooseBlankPage() {
        this.waitUntilExists(this.createPageDialog);
        this.waitForVisibleAndClick(this.blankPage);
        this.waitForVisibleAndClick(this.createPage);
    }


}

export default new PageCreatorPage();