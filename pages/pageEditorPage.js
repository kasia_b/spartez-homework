import Page from './page';
import {Restriction} from "./confluencePage";
import RestrictionPage from '../pages/restrictionsPage';

class PageEditorPage extends Page {

    get contentTitle() {
        return '#content-title';
    }

    get publishButton() {
        return '#rte-button-publish';
    }

    get textFrame() {
        return '#wysiwygTextarea_ifr';
    }

    get textArea() {
        return '#tinymce';
    }

    setupPage(page) {
        this.setTitle(page.title);
        if (page.content) {
            this.editContent(page.content, true);
        }
        if (page.restriction !== Restriction.DEFAULT) {
            RestrictionPage.setRestriction(page.restriction);
        }
        this.save();
    }

    save() {
        this.waitForEnabledAndClick(this.publishButton);
    }


    setTitle(title) {
        this.waitForVisibleAndSet(this.contentTitle, title);
    }

    editContent(text, dontSave) {
        this.waitForAndNavigateToFrame(this.textFrame);
        this.waitForVisibleAndSet(this.textArea, text);
        this.waitForAndNavigateToFrame();
        if (!dontSave) {
            this.save();
        }
    }
}

export default new PageEditorPage();