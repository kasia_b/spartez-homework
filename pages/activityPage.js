import Page from "./page";
import PageCreator from "./pageCreatorPage";
import PageEditor from "./pageEditorPage";
import PageViewer from "./pageViewerPage";

class ActivityPage extends Page {

    constructor() {
        super("Dashboard - Confluence");
    }

    get createPageButton() {
        return '#create-page-button'
    }

    get contentTitle() {
        return '.update-item-title'
    }

    createBlankPage(page) {
        this.waitForVisibleAndClick(this.createPageButton);
        PageCreator.chooseBlankPage();
        PageEditor.setupPage(page);
    }

    openPageInEditMode(page) {
        this.openPageInViewMode(page);
        PageViewer.openEditMode();
    }

    openPageInViewMode(page) {
        this.waitForVisibleAndClick(`=${page.title}`);
    }

    waitForContent() {
        this.waitUntilVisible(this.contentTitle);
    }

}

export default new ActivityPage();