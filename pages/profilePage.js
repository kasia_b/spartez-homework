import Page from "./page";
import LoginPage from "./loginPage";

class ProfilePage extends Page {

    get profileButton() {
        return 'div[data-webitem-location="system.user"]'
    }

    get logoutButton() {
        return 'a[href="/wiki/logout.action"]'
    }

    get logoutSubmitButton() {
        return '#logout-submit'
    }

    logout() {
        this.waitForVisibleAndClick(this.profileButton);
        this.waitForVisibleAndClick(this.logoutButton);
        this.waitForVisibleAndClick(this.logoutSubmitButton);
        this.waitUntilVisible(LoginPage.username);
    }
}

export default new ProfilePage();