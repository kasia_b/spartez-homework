import Page from './page';
import ActivityPage from './activityPage';

class LoginPage extends Page {

    constructor() {
        super("Log in to continue - Log in with Atlassian account");
    }

    get username() {
        return '#username';
    }

    get password() {
        return '#password';
    }

    get submit() {
        return '#login-submit';
    }

    open() {
        super.open('');
        return this;
    }

    login(username, password) {
        let waitSetClick = (element, value) => {
            this.waitForVisibleAndSet(element, value);
            this.waitForVisibleAndClick(this.submit);
        };
        this.open();
        waitSetClick(this.username, username);
        waitSetClick(this.password, password);
        ActivityPage.waitForContent();
    }

}

export default new LoginPage();