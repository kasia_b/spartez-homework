import LoginPage from '../pages/loginPage';
import ActivityPage from '../pages/activityPage';
import PageViewerPage from '../pages/pageViewerPage';
import PageEditorPage from '../pages/pageEditorPage';
import ProfilePage from '../pages/profilePage';
import {ConfluencePage, Restriction} from '../pages/confluencePage';
import ConfluenceSpace from '../pages/confluenceSpace';
import SpacePage from '../pages/spacePage';

import chai from 'chai';
let expect = chai.expect;

describe('Page creation ', function () {

    const space = new ConfluenceSpace('CT', 'Cat trees');

    beforeEach(() => {
        LoginPage.login('gabka.krystyna@gmail.com', 'julianek1');
    });

    afterEach(() => {
        ProfilePage.logout();
    });

    it("creates a new page ", () => {
        let page = new ConfluencePage();
        ActivityPage.createBlankPage(page);
        assertPageExists(page);
    });

    it("creates a new page without restrictions", () => {
        let page = new ConfluencePage();
        ActivityPage.createBlankPage(page);
        ProfilePage.logout();
        LoginPage.login('wyplosz_kot@trash-mail.com', 'wyploszek1');
        ActivityPage.openPageInEditMode(page);
        let content = 'kotek';
        PageEditorPage.editContent(content);
        assertPageContent(content);
    });

    it("creates a new page with restriction to editing", () => {
        let page = new ConfluencePage(Restriction.NOEDIT);
        ActivityPage.createBlankPage(page);
        ProfilePage.logout();
        LoginPage.login('wyplosz_kot@trash-mail.com', 'wyploszek1');
        ActivityPage.openPageInViewMode(page);
        expect(PageViewerPage.canEdit()).to.be.equal(false);
    });

    it("creates a new page with restriction to viewing", () => {
        let page = new ConfluencePage(Restriction.NOVIEW);
        ActivityPage.createBlankPage(page);
        ProfilePage.logout();
        LoginPage.login('wyplosz_kot@trash-mail.com', 'wyploszek1');
        expect(new SpacePage(space).getListOfPages()).to.not.include(page.title);
    });
});


let assertPageExists = (page) => {
    return Promise.all([
        expect(PageViewerPage.getListOfPages()).to.include(page.title),
        assertPageContent(page.content)
    ]);
};

let assertPageContent = (content) => {
    return expect(PageViewerPage.getContent()).to.equal(content);
};