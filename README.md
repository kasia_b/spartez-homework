# pre-requisites
####install [nvm](https://github.com/creationix/nvm)
#### install node
```bash
nvm install 8.9.0
nvm use
```
####install modules
```bash
npm install
```
####run tests
```bash
npm test
```
#####in headless mode
```bash
npm run headless
```
